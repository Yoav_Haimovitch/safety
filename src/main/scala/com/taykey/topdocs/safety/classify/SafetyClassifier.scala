package com.taykey.topdocs.safety.classify

import scala.collection.mutable.MutableList
import com.taykey.topdocs.safety.main.Article

/**
 * @author yoav
 */
class SafetyClassifier(cfg : SafetyClassifierConfig) {
	final val TITLE_WEIGHT = 2
	final val FOUND = 1
	final val PART_FOUND = 2
	val blackTrie:Map[String,Int]=buildTrie(cfg.blacklist.toSeq)
	val whiteTrie:Map[String,Int]=buildTrie(cfg.whitelist.toSeq)

	def buildTrie(ngrams : Seq[String]):Map[String, Int] = {
		ngrams
		.map(_.replaceAll("[\"']", "")).flatMap { x => Seq(x,x+"s") }
		.flatMap ( x => {
			val $ = new MutableList[(String,Int)]()
			val ps = x.split("-")
			var g = ps(0)
			$+=(g->PART_FOUND)
			for (i <- 1 to ps.length-2) {
				g = g+"-"+ps(i)
				$+=(g->PART_FOUND)
			}
			$+=(x->FOUND)
			$.toList
		}).groupBy(_._1).mapValues(_.map(_._2).min)
	}
	
	def proc(a:Article):Int = getScore(cfg.preproc.procOnArticle(a))
	def proc(title:String,body:String):Int = getScore(cfg.preproc.procOnArticle(Article(title,body)))

	
	def executeOnArticles(input: Iterator[Article]): Iterator[Int] = {
		cfg.preproc.procOnArticles(input).map(getScore)
	}

	case class Res(oldScore:Int,newScore:Int, feats:List[String], text:String) { 
		override def toString():String = "oldScore="+oldScore+" newScore="+newScore+" feats="+feats+" text="+text
	}
	
	def findNgram(trie:Map[String, Int], ps:Array[String],i:Int):Option[(Int,String)] = {
		var g = ps(i)
		if (trie.get(g).isEmpty) return Option.empty
		if (trie.get(g).get == FOUND) return Option(0,g)
		for (j <- 1 to ps.size-i-1) {
			g = g+"-"+ps(i+j)
			if (!trie.contains(g)) {
				return Option.empty
			} else if (trie.get(g).get==FOUND) {
				return Option(j,g)
			}
		}
		return Option.empty
	}

	def findClues(s:String):(Int,Int,List[String],List[String]) = {
		if (s.trim.isEmpty) return (0,0,List(),List())
		val ps = s.split("\\s+",0)
		var tot = 0
		var feats = new MutableList[String]()
		var white_feats = new MutableList[String]()
		var count = 0
		var i = 0
		while (i < ps.size) {
			var t = findNgram(whiteTrie, ps,i)
			if (t.isDefined) {
				white_feats+=t.get._2
				tot+=1+t.get._1
				i+=t.get._1
			} else {
				t = findNgram(blackTrie, ps,i)
				if (t.isDefined) {
					feats+=t.get._2
					count+=1
					tot+=1
					i+=t.get._1
				} else 
					if (!cfg.stoplist.contains(ps(i))) tot+=1
			}
			i+=1
		}
		(count,tot, feats.toList, white_feats.toList)
	}
	def findFeatures(s:String): (Int,List[String]) = {
		val (count,tot, feats, white_feats) = findClues(s)
		(calcScore(count,tot), feats ++ {if (!white_feats.isEmpty) List("WHITE:") ++ white_feats else List()})
	} 
	def getCounts(s:String): (Int,Int) = {
		if (s.trim.isEmpty) return (0,0)
		val (count,tot, feats, white_feats) = findClues(s)
		(count,tot)
	}
	def getScore(a:Article): Int = {
		val (title_count, title_tot) = getCounts(a.title)
		val (desc_count,  desc_tot)  = getCounts(a.body)
		calcScore(title_count*TITLE_WEIGHT+desc_count,title_tot*TITLE_WEIGHT+desc_tot)
	}

	def calcScore(unsafeCount:Int,totalWords:Int) = if (totalWords>0) unsafeCount*10000/totalWords else 0
  
}

