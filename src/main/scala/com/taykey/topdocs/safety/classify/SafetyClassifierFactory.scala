package com.taykey.topdocs.safety.classify

import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import com.taykey.topdocs.safety.main.Builder
import scala.io.Source

object SafetyClassifierFactory {
  	def build(configPath:String): SafetyClassifier = {
  		val path = if (configPath.endsWith("/")) configPath else configPath+"/"
  		val sc = createSparkContext
		val cfg = Builder.buildAll ( fn => sc.textFile(path+fn).collect.toSeq )
		sc.stop
		new SafetyClassifier(cfg)
	}
  	
  	private def createSparkContext():SparkContext = {
  		val sparkConf = new SparkConf().setAppName("SafetyClassifierFactory")	
  		sparkConf.setMaster("local[*]")
		val sc = new SparkContext(sparkConf)
  		sc.setLogLevel("ERROR")
		val hadoopConf = sc.hadoopConfiguration
		hadoopConf.set("fs.s3n.awsAccessKeyId", "AKIAI4GXGXUW62TWI6JA")
		hadoopConf.set("fs.s3n.awsSecretAccessKey", "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")
		sc
  	}
  	
}
