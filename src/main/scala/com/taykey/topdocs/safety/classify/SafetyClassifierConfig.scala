package com.taykey.topdocs.safety.classify

import com.taykey.topdocs.safety.preproc.Preproc


/**
 * @author yoav
 */
case class SafetyClassifierConfig(
		threshold: Int,
		preproc:Preproc,
		neverlist:Set[String],
		blacklist:Set[String],
		whitelist:Set[String],
		stoplist:Set[String]
	)
