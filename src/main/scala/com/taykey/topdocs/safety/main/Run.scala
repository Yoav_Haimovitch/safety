package com.taykey.topdocs.safety.main

import java.io.BufferedWriter
import java.io.FileWriter
import scala.io.Source
import com.taykey.topdocs.safety.classify.SafetyClassifier
import org.apache.commons.lang3.StringUtils
import org.apache.spark.SparkConf
import org.apache.spark.SparkContext
import com.taykey.topdocs.safety.classify.SafetyClassifierFactory

/**
 * @author yoav
 */
object Run {
   	def main (args: Array[String]): Unit = {
   		val safetyClassifier:SafetyClassifier = SafetyClassifierFactory.build("s3n://tk-research/safety/")
   		val score:Int = safetyClassifier.proc("title","body")
   	}

   	def fromFiles (configPath:String,inputFile:String): Unit = {
		val cfg = Builder.buildAll ( fn => Source.fromFile(configPath+"/"+fn).getLines.toSeq )
		val c = new SafetyClassifier(cfg)
		val input = Source.fromFile(inputFile).getLines.map{
			x=>	Article(StringUtils.substringBefore(x, "\t"), StringUtils.substringAfter(x, "\t"))
		}.toSeq
		input.map(c.proc)
	}
   	
}