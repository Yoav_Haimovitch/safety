package com.taykey.topdocs.safety.main

import com.taykey.topdocs.safety.classify.SafetyClassifierConfig
import com.taykey.topdocs.safety.preproc.Preproc
import com.taykey.topdocs.safety.preproc.Preproc.Replacer

/**
 * @author yoav
 */
object Builder {
	def buildAll(lineGetter: String=>Seq[String]):SafetyClassifierConfig = {
		val cfg = lineGetter("safety.cfg")
		val neverlist = lineGetter("neverlist.txt")
		val blacklist = lineGetter("full_blacklist.txt")
		val whitelist = lineGetter("full_whitelist.txt")
		val stoplist = lineGetter("stopwords")
		val hashtags = lineGetter("unsafe_hashtags.txt").map(s=>"#"+s)
		val upper =  "\\p{Lu}".r
		val capsWords = (neverlist++blacklist++whitelist++stoplist).filter { upper.findFirstIn(_).isDefined }
		val preproc = buildPreproc(lineGetter, capsWords)

		def prep(seq:Seq[String]):Seq[String]={
			preproc.normalize(seq).map(_.toLowerCase).map(preproc.handleQuotes).map(_.trim).map(_.replaceAll(" ","-"))
		}
		val normedBlacklist = prep(blacklist)
		val normedNeverlist = prep(neverlist)
	
		buildConfig(cfg, preproc,
				normedNeverlist++normedNeverlist.map(hash_tagify),
				normedBlacklist++normedBlacklist.map(hash_tagify)++hashtags,
				whitelist,
				stoplist)
	}
	
	def buildConfig(cfg: Seq[String], preproc:Preproc, neverlist: Seq[String], blacklist: Seq[String], whitelist: Seq[String], stoplist: Seq[String]): SafetyClassifierConfig = {
		val config = cfg.map {x=>val ps = x.split("\\t+"); (ps(0),ps(1)); }.toMap
		val joinedBlacklist = neverlist++blacklist

		new SafetyClassifierConfig(
			config.get("threshold").get.toInt,
			preproc,
			neverlist.toSet,
			joinedBlacklist.toSet,
			whitelist.toSet,
			stoplist.toSet
		)
	}
	
	def hash_tagify(s:String):String = {
		if (s.startsWith("#")) return s
		"#"+s.replace("-","")
	}

	def buildPreproc(lineGetter: String=>Seq[String], capsWords: Seq[String]): Preproc = {
		val capsRegex = (capsWords.map {
			x => (("\\b"+x+"\\b").r,x.replaceAll("""\b([\w\.]*\p{Lu}[\w\.]*)\b""", "__$1__").toLowerCase)
		})
		new Preproc(
				buildReplacer(lineGetter("replace0_norm.cfg"))++capsRegex,
				buildReplacer(lineGetter("replace1_punct.cfg"))
		)
	}

	def buildReplacer(lines:Seq[String]):Replacer = {
		lines.filter(!_.trim.isEmpty()).map {x=>
			val ps = x.split("\\t+");
			var v = ""
			if (ps.length == 2) v = ps(1) 
			(ps(0).r, v);
		}
	}  
}