package com.taykey.topdocs.safety.main
import com.fasterxml.jackson.databind.JsonNode
import scala.io.Source
import com.taykey.scala.framework.utils.JsonParser
import java.text.Normalizer
import org.apache.commons.lang3.StringEscapeUtils
import java.io.FileWriter
import java.io.BufferedWriter
/*
* @author yoav
 */
object TopdocJsonParser {
	def main (args: Array[String]): Unit = {
//		val args2 = Seq("/data/topdocs/raw/2016")
		for (fn <- args) {
//		val fn = "/data/topdocs/raw/end2015.raw"
			val bw = new BufferedWriter(new FileWriter(fn+".out"))
			val lines = Source.fromFile(fn).getLines()
			val jsonString = lines.next()
			val it = JsonParser.fromJson(jsonString).path("hits").path("hits").elements()
			while (it.hasNext()) {
				bw.write (extractStrings(it.next))
				bw.newLine
				bw.write ("###")
				bw.newLine
			}
			bw.close
		}
	}
	
	def extractStrings(json:JsonNode) :String = {
		val id = json.path("_id").asText()
		val source = json.path("_source").path("source").asText
		val link = json.path("_source").path("link").asText
		val it = json.path("_source").path("documentParts").elements()
		val $ = new TopDoc
		while (it.hasNext()) {
			val p = it.next
			val content = p.path("content").asText()
			val partId = p.path("partId").asInt
			val lang = p.path("language").asText()
			if (lang == "en") p.path("type").asText() match {
				case "title" => { $.title.clean(content,partId) }
				case "crawled.title" => { $.title.crawled(content,partId) }
				case "description" => {	$.desc.clean(content,partId) }
				case "crawled.description" => {	$.desc.crawled(content,partId) }
				case "keywords" => { $.keywords.clean(content,partId) }
				case "crawled.keywords" => { $.keywords.crawled(content,partId) }
				case _ => {}
			}
		}
		
		return "id="+id+"\n"+
			"source="+source+"\n"+
			"link="+link+"\n"+
//			"entities="+extractEnts(json.path("_source").path("entities"))+"\n"+
			"aliases="+extractAliases(json.path("_source").path("entities"))+"\n"+
			$.toString()
	}

	import collection.JavaConversions._
	def extractEnts(json:JsonNode) : String = {
		return json.toString()
		val $ = json.elements()
			.map { x => x.path("id").asInt }
			.toSet
			.mkString(" ")
		$
	}
	def extractAliases(json:JsonNode) : String = {
		val $ = json.elements()
			.flatMap { x => x.path("documentParts").elements() }
			.flatMap { x => x.path("aliases").elements() }
			.map { x => x.asText.replaceAll(" ", "-") }
			.toSet
			.mkString(" ")
		$
	}

}

class TopDoc {
	val title=new Datum()
	val desc=new Datum()
	val keywords=new Datum()
	
	override def toString():String = {
		Seq("title="+title.toString,"desc="+desc.toString,"keywords="+keywords.toString).mkString("\n")
	}
}

class Datum {
	var clean   = ""
	var clean_partId = -1
	
	var crawled = ""
	var crawled_partId = -1
	
	def clean(v:String, id:Int):Unit = {
		if (v.length() > clean.length()) clean = v
	}
	def crawled(v:String, id:Int):Unit = {
		if (v.length() > crawled.length()) crawled = v
	}
	
	override def toString():String = {
		if (clean != "") return c(clean)
		if (crawled != "") return c(crawled)
		return "NA"
	}
	
	def c(text:String) : String = {
		Normalizer.normalize(StringEscapeUtils.unescapeHtml4(text), Normalizer.Form.NFKD)
		.replaceAll("\\p{InCombiningDiacriticalMarks}+", "")
		.replaceAll("[\\r\\n]+"," <BR/> ")
		.replaceAll("[\\p{Blank}\\p{IsControl}]+", " ")
//		.replaceAll("[ʺ“”„‟″‶〃〝〞]", "\"")
//		.replaceAll("[´ʹʼˈˋ‘’‛′‴‵‷`]", "'")
//		.replaceAll("[­‐‑‒–—―−]", "-")
	}

}
