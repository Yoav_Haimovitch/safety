package com.taykey.topdocs.safety.main

case class Article(val title:String, val body:String) {
	override def toString():String = "TITLE:'"+title+"'  BODY:'"+body+"'"
}