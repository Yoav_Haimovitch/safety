package com.taykey.topdocs.safety.preproc

import scala.util.matching.Regex
import java.text.Normalizer
import org.apache.commons.lang3.StringEscapeUtils
import com.taykey.topdocs.safety.preproc.Preproc.Replacer
import com.taykey.topdocs.safety.main.Article

/**
 * @author yoav
 */
object Preproc {
	type Replacer = Seq[(Regex, String)]
}
class Preproc(normReplacer : Replacer, punctReplacer : Replacer) {

	def procOnArticle(a:Article):Article = {
		val x = proc(List(a.title,a.body)).toArray; Article(x(0),x(1))
	}

	def procOnArticles(as:Iterator[Article]): Iterator[Article] = as.map(procOnArticle)
	
	def proc(texts:Seq[String]):Seq[String] =  proc(texts.iterator).toSeq
	def proc(texts:Iterator[String]):Iterator[String] = {
		normalize(texts)
		.map(interProc)
		.map(_.toLowerCase)
		.map(handleQuotes)
		.map(doReplace(punctReplacer,_))
		.map(handleNonWords)
	}
	
	def normalize(texts:Seq[String]):Seq[String] = {
		normalize(texts.iterator).toSeq
	}
	def normalize(texts:Iterator[String]):Iterator[String] = {
		texts.map(StringEscapeUtils.unescapeHtml4)
		.map(Normalizer.normalize(_,Normalizer.Form.NFKD))
		.map(doReplace(normReplacer,_))
	}

	def doReplace(replacer : Replacer, s:String): String = {
		var $ = s;
		for (it <- replacer) try {
			$ = it._1.replaceAllIn($, it._2)
		} catch {
			case e:Exception => {
				System.err.println(it.toString())
				throw e
			}
		}
		$
	}

	def interProc(s: String):String = {
		val capsSpace = """\b[A-Z](?: [A-Z]){2,}\b""".r
		var $ = s
		var m = capsSpace.findFirstIn($)
		while (m.isDefined) {
			$ = $.replace(m.get, m.get.replaceAll(" ", ""))
			m = capsSpace.findFirstIn($)
		}
		$
	}

	def handleQuotes(s:String):String = {
		//s.replaceAll("""(?<=\w)'(?=\w)""", "__Apostrophe__").replaceAll("['\"]"," \" ").replaceAll("__Apostrophe__", "'")
		s.replaceAll("['\"]","")
	}

	def handleNonWords(s: String):String = {
		(" "+s+" ")
			.replaceAll("[^A-Za-z0-9\\.,;\\-_\\(\\)\\[\\]\\?!] ", " ")
			.replaceAll("([,;\\.\\)\\]]) ", " $1 ")
			.replaceAll(" ([,;\\.\\[\\(])", " $1 ")
			.replaceAll("(?<=\\w)-(?=\\w)"," ")
			.replaceAll(" +"," ")
			.trim
	}

}