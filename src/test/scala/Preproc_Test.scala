

import org.junit.runner.RunWith
import org.scalatest._
import org.scalatest.junit.JUnitRunner
import collection.JavaConversions._
import scala.io.Source
import com.taykey.topdocs.safety.main.Builder

@RunWith(classOf[JUnitRunner])
class Preproc_Test extends FunSuite with Matchers with BeforeAndAfter {
	 test ("Preproc") {
		val p = Builder.buildAll(fn => { 
			if (fn == "full_blacklist.txt") Seq("AIDS") else
			Source.fromFile("/data/ontology/unsafe/"+fn).getLines().toSeq
		}).preproc
		val actual = p.proc(Seq("He aids with the AIDS research"))(0)
		actual should be ("he aids with the __aids__ research")
  }
}
